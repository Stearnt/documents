\input{preamble}

\title{Algèbre - Produit scalaire sur un espace vectoriel - Espace préhilbertien}
\author{Edgar Bizel}

\begin{document}
     \maketitle

     \section{Généralités sur les espaces préhilbertiens}

     \subsection{Forme linéaire}

     On peut justifier la linéarité :
     \begin{itemize}
         \item Par la définition
         \item Par l'exhibition d'une matrice, uniquement de $\R^n$ dans $\R^p$
     \end{itemize}
     
     \subsection{Produit scalaire}

     \begin{exos}
         Questions à traiter seul

         Soit $\Phi$ un produit scalaire sur $E$.

         \begin{enumerate}
             \item $\Phi( 0_E, 0_E )=0$
             \item Si $u \neq 0_E$ alors $\Phi( u,u )>0$ 
             \item Soit $u \in E, \Phi( u, 0_E )= \Phi( 0_E, u )=0$
         \end{enumerate}
     \end{exos}

    \begin{exo}
        \textbf{Exercice page 39}

        On doit montrer : 
        \begin{align*}
            \sum_{k=0}^{n} \sqrt{\binom{ n }{ k }} \le \sqrt{2^n ( n+1 )}
        \end{align*}

        \highlighty{Analyse}. On cherche $u,v \in \R^{n+1}$ tels que :
        \begin{align*}
            \abs{( u \mid v )}= \sum_{k=0}^{n} \sqrt{\binom{ n }{ k }} \quad \norme{ u } =
            \sqrt{2^n} = \sqrt{( 1+1 )^n} \quad \norme{ v } = \sqrt{n+1}
        \end{align*}

        \highlighty{Synthèse}. 

        On pose : $u=\left( \sqrt{\binom{ n }{ 0 }}, \ldots, \sqrt{\binom{ n }{ k }},
        \ldots, \sqrt{\binom{ n }{ n }} \right) \quad v = ( 1,\ldots,1,\ldots,1 )$ 

        On calcule : 
        \begin{align*}
            ( u \mid v ) &= u \\
            \norme{ u } &= \sum_{k=0}^{n}\sqrt{\binom{ n }{ k }} = \sqrt{( 1+1
            )^n}=\sqrt{2^n}\\
            \norme{ v } &= \sqrt{n+1}
        \end{align*}
        On déduit par ICS : $ \abs{( u \mid v )} \le \norme{ u }\times \norme{ v } \iff
        \sum_{k=0}^{n} \sqrt{\binom{ n }{ k }} \le \sqrt{2^n( n+1 )}$
    \end{exo}

    \begin{exo}
        Avec le produit scalaire usuel

        \begin{enumerate}
            \item Soit $u=( 1,2 ) \in \R^2$. Normaliser $u$.

            \begin{align*}
                \norme{ u }&= \sqrt{( u \mid v )}\\
                &= \sqrt{1^2+2^2} \\
                &= \sqrt{5}
            \end{align*}
            On pose : 
            \begin{align*}
                \hat{u}=\frac{1}{\sqrt{5}} \cdot u
            \end{align*}
            Le vecteur $\hat{u}$ est unitaire.

            \item Soient $u,v \in \R^n$, non nuls.

            Développer $\norme{ u+v }^2$ en fonction de $\norme{ u }, \norme{ v }$ et de
            $\rho( u,v )$.

            \begin{align*}
                \norme{ u+v }^2 &= ( u+v \mid u+v ) \\
                &= ( u \mid u )+( v \mid v )+( u \mid v )+( v \mid u ) \\
                &= \norme{ u }^2+\norme{ v }^2+ 2 \underbrace{\rho ( u,v ) \norme{ u
                }\norme{ v }}_{( u \mid v )}
            \end{align*}

            \item Quelle relation obtient-on dans les cas particuliers où : $u, v$ sont
                colinéaires ? $u,v $ orthogonaux ?

            Si $u,v$ colinéaires alors posons : $v=\lambda u$ avec $\lambda \neq 0, u \neq
            0_E$ 

            D'une part : $\norme{ u+v }^2 = \norme{ ( 1+\lambda )u }^2 = ( 1+\lambda )^2
            \norme{ u }^2$ 

            D'autre part : $\norme{ u+v }^2 = \norme{ u }^2+\norme{ v }^2 + 2\rho ( u,v )
            \norme{ u }\norme{ v }= ( 1+\lambda^2+2\rho( u,v ) \abs{\lambda}  ) \norme{ u
            }^2$

            En identifiant : $\lambda = \rho( u,v ) \abs{\lambda} \iff \rho ( u,v ) = \pm
            1$

            Si $u,v$ orthogonaux alors : $( u \mid v )=0$ 

            Si $u,v$ non nuls alors : $\rho( u,v ) \norme{ u }\norme{ v } = 0$ donc $\rho(
            u,v)=0$
        \end{enumerate}
    \end{exo}

    \textbf{Identités de polarisation}

    Elles permettent de passer d'informations sur des normes à des informations sur le
    produit scalaire, et réciproquement

    \begin{demo}
        \textbf{Identités de polarisation page 9}

        \begin{enumerate}
            \item 
            \begin{align*}
                \norme{ u+v }^2 &= ( u+v \mid u+v ) = \norme{ u }^2+2( u \mid v )+\norme{
                v}^2\\
                \norme{ u-v }^2 &= ( u-v \mid u-v ) = \norme{ u }^2-2( u \mid v )+\norme{
                v}^2 \\
                \shortintertext{D'où :}
                ( u \mid v ) &= \frac{1}{2}( \norme{ u+v }^2 - \norme{ u }^2-\norme{ v }^2
                )\\
                ( u \mid v ) &= \frac{1}{2}( \norme{ u }^2+\norme{ v^2 }-\norme{ u-v }^2
                )\\
                ( u \mid v )&= \frac{1}{4}( \norme{ u+v }^2-\norme{ u-v }^2 )
            \end{align*}
        \end{enumerate}
    \end{demo}

    \begin{exo}
        Montrer la proposition suivante :
        \begin{align*}
            \forall a, b \in \R, ( a>0 \et b > 0 ) \iff ( \forall ( x,y ) \in \R^2
            \setminus \set{ ( 0,0 ) }, ax^2 + by^2 > 0 )
        \end{align*}

        Soient $a, b \in \R.$ Supposons $ a>0 \et b > 0$.

        Soit $( x,y ) \in \R^2\setminus \set{ ( 0,0 ) } $. Donc :
        \begin{align*}
            0 < x^2+y^2 \text{ car $x,y$ non tous nuls}\\
            \shortintertext{Supposons $a\le b$}
            \donc : 
            \left\{
            \begin{NiceMatrix}
                0 < ax^2+ay^2\\
                0 < ay^2\le by^2
            \end{NiceMatrix}
            \right.
        \end{align*}

        \TODO
    \end{exo}

    \begin{exo}
        \textbf{Exercice 2.3 page 13}

        \begin{align*}
            \Phi: \R^2\times \R^2 &\longrightarrow \R \\
            ( u,v ) &\longmapsto 13x x' - 3xy' -3x'y+yy' 
        .\end{align*}

        \begin{itemize}
            \item Vérifier $\Phi( u,v ) = l_1( u )l_1( v )+4l_2( u )l_2( v )$

            \begin{align*}
                l_1: \R^2 &\longrightarrow \R \\
                ( x,y ) &\longmapsto 3x-y 
            .\end{align*}
            \begin{align*}
                l_2: \R^2 &\longrightarrow \R \\
                ( x,y ) &\longmapsto x 
            .\end{align*}
            \begin{align*}
                l_1( u )l_1( v )+4l_2( u )l_2( v ) &= ( 3x-y )( 3x'-y' )+4x x' \\
                &= ( 9+4 )x x'-3xy'-3yx'+yy' \\
                &= \Phi( u,v )
            \end{align*}

            \item Déduire que $\Phi$ est un produit scalaire

            $l_1, l_2$ non colinéaires donc $rg( \set{ l_1,l_2 }=2$.

            Donc $ \set{ l_1,l_2 } $ est une famille libre et même une base de $ \mathcal{L} (
            \R^2, \R)$

            De plus les coefficients 1 et 4 sont strictement positifs. 

            Donc $\Phi$ est un produit scalaire sur $\R^2$.

            \item 

            On fixe la 1ere variable de $\Phi$. Donc $\Phi( e_1, \cdot  ) \in \mathcal{L}(
            \R^2, \R )$ 
            \begin{align*}
                \forall u = ( x,y ) \in \R^2, \Phi( e_1, u ) = 13x-3y
            \end{align*}
            $\Phi( e_1,\cdot  )$ est lipschitzienne donc continue dans $( \R^2, \norme{ \cdot
            }_\infty )$ 

            Dans $\R^2$, les normes sont équivalentes.

            Donc : $\Phi( e_1,\cdot  )$ est continue dans $( \R^2, \Phi )$.

            Tout singleton $\set{ 5k } $ est un fermé de $( \R, \abs{\cdot } )$ 

            Donc : $\Phi( e_1,\cdot  )^{-1}( \set{ 5k }  )$ est une partie fermée de $( \R^2,
            \Phi)$

            \item

            Déterminons les images réciproques par $\Phi$ des fermés $\set{ 0 } $ et $\set{ 5 } $ 

            \begin{align*}
                \Phi( e_1, \cdot  )^{-1}( \set{ 0 }  ) &= \set{ ( x,y ) \in \R^2, 13x-3y=0 }  = Vect( ( 3,13
                ) )\\
                \Phi( e_1,\cdot  )^{-1}( \set{ 5 }  )&=\set{ ( x,y )\in \R^2, 13x-3y=5 }  =
                \set{ ( 0,-\frac{5}{3} ) + \lambda ( 3,13 ), \lambda \in \R } 
            \end{align*}

        \end{itemize}
    \end{exo}

    \begin{expl}
        \textbf{Exemple page 22 (du diapo)}

        $Vect( u_1,u_2,u_3 )^\perp = \set{ u_1,u_2,u_3 }^\perp$ ?

        C'est vrai, par bilinéarité de $\phi$.

        Soit $a \in Vect( u_1,u_2,u_3 )^\perp$.
        \begin{align*}
            \forall u \in Vect( u_1,u_2,u_3 ), &\phi( u,a )=0
        \end{align*}
        En particulier : $\phi( u_1,a )=0, \phi( u_2,a )=0, \phi( u_3,a )=0$ 

        Donc : $a \in \set{ u_1,u_2,u_3 }^\perp$ 

        \bigskip

        Soit $a \in \set{ u_1,u_2,u_3 }^\perp$. Soit $u \in Vect( u_1,u_2,u_3 )$.
        Calculons $\phi( u,a )$ 
        \begin{align*}
            \exists !( \lambda_1,\lambda_2,\lambda_3 ) \in \R^3, u = \lambda_1 u_1 +
            \lambda_2 u_2 + \lambda_3 u_3 \\
            \phi( u,a ) = \lambda_1 \underbrace{\phi( u_1,a )}_{=0} + \lambda_2
            \underbrace{\phi( u_2,a )}_{=0} + \lambda_3 \underbrace{\phi( u_3,a )}_{=0}
        \end{align*}
        Donc : $a \in Vect( u_1,u_2,u_3 )^\perp$
    \end{expl}

    \begin{expl}
        \textbf{Exemple 4.1 page 22}

        Soit $u=( 1,1,1 ) \in \R^3$ 

        Déterminons $Vect( u )^\perp$ par rapport à chaque produit scalaire :
        \begin{itemize}
            \item Avec le produit scalaire usuel $p_1$ : $Vect( u )^\perp = \set{ ( x,y,z )\in
            \R^3, x+y+z=0 } $ 

            $Vect( u )^\perp = Vect( ( -1,1,0 ), ( -1,0,1 ) )$

            Détails : soit $( x,y,z ) \in \R^3$.
            \begin{align*}
                ( x,y,z ) \in Vect( u )^\perp \iff& ( x,y,z ) \in \set{ u }^\perp \\
                \iff& p_1( ( x,y,z ), u )=0 \\ 
                \iff& x \times 1+y \times 1 + z \times 1=0 \\ 
                \iff& x = -y-z\\
                \iff& ( x,y,z )= ( -y-z, y,z )\\
                \iff& ( x,y,z )=y( -1,1,0 )+z( -1, 0,1 ) \\ 
                \iff& ( x,y,z ) \in Vect( ( -1,1,0 ),( -1,0,1 ) ) 
            \end{align*}


            \item Avec 
            \begin{align*}
                p_2: \R^3\times \R^3 &\longrightarrow \R \\
                ( ( x_1,y_1,z_1 ),( x_2,y_2,z_2 ) ) &\longmapsto x_1x_2 + 2y_1y_2+z_1z_2 
            .\end{align*}
            On a : $Vect( u )^\perp = \set{ ( x,y,z ) \in \R^3, x+2y+z=0 } $

            $Vect( u )^\perp = Vect( ( -2,1,0 ),( -1,0,1 ) )$
        \end{itemize}
    \end{expl}

    \begin{exo}
        \textbf{Exercice 4.1 page 23}

        \begin{enumerate}
            \item 
            \begin{enumerate}
                \item Vérifier que $\set{ a,b } $ est une famille libre de $E$ 

                Soit $\lambda, \mu \in \R$.
                \begin{align*}
                    \lambda a  +\mu b = 0 \iff& \norme{ \lambda a + \mu b }_\Phi = 0 \text{
                    car $\Phi$ est défini}\\
                    \iff& \Phi( \lambda a + \mu b, \lambda a + \mu b  )=0\\
                    \iff& \lambda^2 \norme{ a }^2_\Phi + \mu^2 \norme{ b }_\Phi^2 +
                    2\lambda \mu \Phi( a,b )=0 \text{ car $\Phi$ bilinéaire} \\
                    \iff& \lambda^2+\mu^2=0 \text{ car $\set{ a,b } \phi$-orthonormée }\\
                    \iff& \lambda^2 =\mu^2 \\
                    \iff& \mu=\lambda=0 \text{ car $\R$ intègre}
                \end{align*}
                \Conclusion : $\set{ a,b } $ est une famille libre de $E$.

                \item Montrer : $Vect( a,b )^\perp = \set{ a,b }^\perp$ 

                Soit $u \in E$.
                \begin{align*}
                    u \in Vect( a,b )^\perp \iff& \forall \lambda,\mu \in \R ,\Phi(
                    u,\lambda a + \mu b )=0\\
                    \donc& \Phi( u,a )=0 \et \Phi( u,b )=0\\
                    \donc& u \in \set{ a,b }^\perp
                \end{align*}
                \begin{align*}
                    u \in \set{ a,b }^\perp \donc& \forall \lambda, \mu \in \R \Phi(
                    u,\lambda a + \mu b )=0 \text{ car $\Phi$ bilinéaire}\\
                    \donc& u \in Vect( a,b )^\perp
                \end{align*}
                \Conclusion : $Vect( a,b )^\perp=\set{ a,b }^\perp$
                
                \item Déterminer le noyau de $f$

                Soit $u \in E$.
                \begin{align*}
                    u \in Ker( f ) \iff& f( u )=0_E\\
                    \iff& \Phi( a,u ) a - \Phi( b,u ) b = 0_E\\
                    \iff& \Phi( a,u ) = 0 \et \Phi( b,u ) = 0 \text{ car $\set{ a,b } $
                    libre}\\
                    \shortintertext{D'une part :}
                    u \in Ker( f ) \iff& u \in \set{ a,b } ^\perp\\
                    \shortintertext{D'autre part :}
                    u \in Ker( f ) \iff& u \in \underbrace{Ker( \Phi( a,\cdot ) )\cap Jer(
                    \Phi( b,\cdot  ))}_{2 \text{ hyperplans}}\\
                    \text{Car } \Phi( a,\cdot  ),\Phi( b,\cdot  ) \in \mathcal{L}( E,\R
                    )\setminus \set{ \tilde{ 0 } }\\
                    u \in Ker( f ) \iff& u \in Vect( a )^\perp \cap Vect( b )^\perp
                \end{align*}
                \Conclusion : $Ker( f )=\set{ a,b }^\perp = Vect( a )^\perp \cap Vect( b
                )^\perp$  
            \end{enumerate}
            \item Appliquer à $(\R^3, ( \cdot  \mid \cdot  ))$ avec $a=( 1,0,0 ), b=(
                0,1,0 )$ 

            Soit $u=( x,y,z )\in \R^3$.
            \begin{align*}
                u \in Ker( f ) \iff& u \in Vect( a )^\perp \cap Vect( b )^\perp\\
                \iff& x=0 \et y =0\\
                \iff& u=( 0,0,z )\\
                \iff& u \in Vect( ( 0,0,1 ) )
            \end{align*}

            \item Dans $\left( \R[X], \int_{0}^{1} \cdot x\cdot   \right)$, on pose
                $a=\tilde{ 1 }, b=\sqrt{3}( 2X-1 )$

            \begin{enumerate}
                \item Vérifier que $\set{ a,b } $ orthonormé.
                \begin{align*}
                    \norme{ a }_\Phi^2 &= \int_{0}^{1} 1^2dt = 1  \\
                    \norme{ b }_\Phi^2 &= \int_{0}^{1} (\sqrt{3}( 2t-1 ))^2 dt =     1  \\
                    \Phi( a,b )&= \int_{0}^{1} \sqrt{3}( 2t-1 )dt = 0 
                \end{align*}

                \item Appliquer $( 1 ) $ en associant à tout polynôme sa fonction
                    polynomiale continue sur $\intervalleff{0}{1}$

                Soit $P \in \R[X]$.
                \begin{align*}
                    P \in Vect( a )^\perp \iff& 
                \end{align*}
                \TODO
            \end{enumerate}
        \end{enumerate}
    \end{exo}
\end{document}
